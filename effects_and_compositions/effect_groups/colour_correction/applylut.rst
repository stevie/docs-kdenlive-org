.. metadata-placeholder

   :authors: - Mmaguire (https://userbase.kde.org/User:Mmaguire)

   :license: Creative Commons License SA 4.0

.. _applylut:

Apply LUT
=========

.. contents::

This is the `Avfilter lut3d <https://www.mltframework.org/plugins/FilterAvfilter-lut3d/>`_ MLT filter.

Apply a 3D Look Up Table (LUT) to the video. A LUT is an easy way to correct the color of a video.

**Supported formats:**

.3dl (AfterEffects), .cube (Iridas), .dat (DaVinci), .m3d (Pandora)

**Parameters:**

Filename: File containing the LUT to be applied.

Interpolation Method: Can be Nearest, Trilinear or Tetrahedral. Defaults to Tetrahedral.

