.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Roger (https://userbase.kde.org/User:Roger)

   :license: Creative Commons License SA 4.0

.. _download_new_render_profiles:

Download New Render Profiles
============================

.. contents::


**Note:** Between August, 2013 and October 2014 this feature had been unavailable and the "Get Hot New Stuff" window will wait forever to update. See Mantis `3133 <https://bugs.kdenlive.org/view.php?id=3133>`_.  From around Oct  2014 this feature has been re-instated. However as at Oct 2015 there are only two profiles available for download.

The workaround for this issue is to use Kdenlive ver 0.9.10 (or higher) and create your own new render profiles and make use of the *melt* property presets – as described  :ref:`render_profile_parameters`.

This feature allows you to download new render profiles that have been shared by the community. These will then appear as options in the :ref:`render` window.


.. image:: /images/Kdenlive_Download_new_render_profiles.png


The new render profile installed above shows up in the Web Sites category under Custom.  


.. image:: /images/Kdenlive_Installed_render_profiles.png


.. note::

  It has the big red cross because the render profile is in need of an audio codec not installed on this machine


The installed files are placed in /usr/share/mlt/presets/consumer/avformat


Upload/Share Render Profiles
============================

If you want to share a render profile you can do so at the `KDE Store page <https://store.kde.org/browse?cat=334>`_. Profiles submitted there appear in the *Download New Render Profiles*.


