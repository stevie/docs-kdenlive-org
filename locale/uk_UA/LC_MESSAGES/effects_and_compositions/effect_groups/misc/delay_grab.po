# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 19:53+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:12
msgid "Delay grab effect"
msgstr "Ефект затримки захоплення"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:14
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:16
msgid "This effect is available from the misc group."
msgstr "Цей ефект доступний з групи «Інше»."

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:18
msgid ""
"This is the `Frei0r delaygrab <https://www.mltframework.org/plugins/"
"FilterFrei0r-delaygrab/>`_ MLT filter by Bill Spinhover, Andreas Scheffler "
"and Jaromil."
msgstr ""
"Це фільтр MLT `frei0r.delaygrab <https://www.mltframework.org/plugins/"
"FilterFrei0r-delaygrab/>`_, який створено Біллом Спінговером, Андреасом "
"Шеффлером та Яромілом"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:20
msgid "Delayed frame blitting mapped on a time bitmap."
msgstr "Затриманий показ кадрів на растровому зображенні."

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:22
msgid "https://youtu.be/t6rsEdDiuAQ"
msgstr "https://youtu.be/t6rsEdDiuAQ"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:24
msgid "https://youtu.be/vh63RxHm8Lg"
msgstr "https://youtu.be/vh63RxHm8Lg"
