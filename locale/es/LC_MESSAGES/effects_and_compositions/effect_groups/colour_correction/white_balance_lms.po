# Spanish translations for docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___white_balance_lms.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___white_balance_lms\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2021-12-25 09:45+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:12
msgid "White Balance (LMS)"
msgstr "Balance de blancos (LMS)"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:14
msgid "Contents"
msgstr "Contenido"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:16
msgid ""
"This is the `Frei0r colgate <https://www.mltframework.org/plugins/"
"FilterFrei0r-colgate/>`_ MLT filter by Steiner H. Gunderson."
msgstr ""
"Este es el filtro `Frei0r colgate <https://www.mltframework.org/plugins/"
"FilterFrei0r-colgate/>`_ de MLT, por Steiner H. Gunderson."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:18
msgid "Do simple color correction, in a physically meaningful way."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:20
msgid "**Parameters:**"
msgstr "**Parámetros:**"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:22
msgid ""
"Neutral Color: Choose a color from the source image that should be white."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:24
msgid ""
"Color Temperature: Choose an output color temperature, if different from "
"6500 K."
msgstr ""
