# Spanish translations for docs_kdenlive_org_effects_and_compositions___effect_groups___crop_and_transform___transform.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___effect_groups___crop_and_transform___transform\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-24 18:33+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:14
msgid "Transform"
msgstr "Transformar"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:18
msgid ""
"This is the `Qtblend <https://www.mltframework.org/plugins/FilterQtblend/>`_ "
"MLT filter."
msgstr ""
"Este es el filtro `Qtblend <https://www.mltframework.org/plugins/"
"FilterQtblend/>`_ de MLT."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:20
msgid "Manipulates Position, scale and opacity."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:22
msgid ""
"The Composition mode parameter of the effect is documented on the Qt "
"documentation under `QPainter CompositionMode <https://doc.qt.io/qt-5/"
"qpainter.html#CompositionMode-enum>`_."
msgstr ""
