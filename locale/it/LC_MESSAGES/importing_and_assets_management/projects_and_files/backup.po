# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-02-09 19:10+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:14
msgid "Backup"
msgstr "Copia di sicurezza"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:16
msgid "Contents"
msgstr "Contenuto"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:23
msgid ""
"The Backup widget, found in :menuselection:`Project --> Open Backup File` "
"allows you to restore a previous version of your project file."
msgstr ""
"L'oggetto per creare la copia di sicurezza, che si trova in :menuselection:"
"`Progetto --> Apri la copia di sicurezza`, ti permette di ripristinare una "
"versione precedente del file del progetto."

#: ../../importing_and_assets_management/projects_and_files/backup.rst:26
msgid ""
"In case something went wrong (corrupted project file, unwanted change, ...), "
"you can now restore a previous version of the file using this feature. Just "
"select the version you want and click :menuselection:`Open`."
msgstr ""
"Nel caso in cui qualcosa vada storto (file del progetto corrotto, modifiche "
"accidentali) puoi ripristinare una versione del file usando questa "
"funzionalità: seleziona semplicemente la versione desiderata, poi fai cli "
"su :menuselection:`Apri`."

#: ../../importing_and_assets_management/projects_and_files/backup.rst:28
msgid ""
"The backup files are automatically created each time you save your project. "
"This means that if you save your project every hour, the backup widget will "
"show you a list of all the saved files, with a small image of the timeline "
"at the time you saved the project."
msgstr ""
"I file della copia di sicurezza vengono creati automaticamente ogni volta "
"che salvi il progetto. Ciò significa che se salvi il progetto ogni ora, "
"l'oggetto per la copia di sicurezza ti mostrerà un elenco di file salvati, "
"ognuno con una miniatura della linea temporale creata nel momento in cui "
"avevi salvato il progetto."

#: ../../importing_and_assets_management/projects_and_files/backup.rst:31
msgid ""
"**Kdenlive** keeps up to 20 versions of your project file in the last hour, "
"20 versions from the current day, 20 versions in the last 7 days and 20 "
"older versions, which should be sufficient to recover from any problem."
msgstr ""
"**Kdenlive** mantiene fino a 20 versioni del file del progetto nell'ultima "
"ora, 20 versioni del giorno corrente, 20 degli ultimi 7 giorni e 20 più "
"vecchie: dovrebbero essere sufficienti per recuperare il file qualsiasi cosa "
"accada."
