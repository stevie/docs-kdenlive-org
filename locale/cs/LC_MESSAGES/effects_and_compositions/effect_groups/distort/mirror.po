# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vpelcak@suse.cz>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-18 09:52+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:13
msgid "Mirror"
msgstr "Zrcadlo"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:15
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:17
msgid ""
"This is the `Mirror <https://www.mltframework.org/plugins/FilterMirror/>`_ "
"MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:19
msgid "Provides various mirror and image reversing effects."
msgstr ""

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:21
msgid "https://youtu.be/ao32j0dSVII"
msgstr "https://youtu.be/ao32j0dSVII"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:23
msgid "https://youtu.be/3-hcMZu52Vk"
msgstr "https://youtu.be/3-hcMZu52Vk"
