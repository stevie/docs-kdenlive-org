# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-12-06 15:27+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../user_interface/menu/clip_menu/clip_in_timeline.rst:18
msgid "Clip In Timeline"
msgstr "Vidéo dans la frise chronologique"

#: ../../user_interface/menu/clip_menu/clip_in_timeline.rst:20
msgid "Contents"
msgstr "Contenu"

#: ../../user_interface/menu/clip_menu/clip_in_timeline.rst:22
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin or under the :ref:`project_menu` menu when a clip is selected in "
"the Project Bin.  It is useful for quickly locating all the places where a "
"clip is used on the timeline."
msgstr ""
"Cet élément de menu est disponible à partir de :ref:`arborescence_projet` "
"sur une vidéo du dossier de projet ou sous le menu :ref:`menu_projet` "
"lorsqu'une vidéo est sélectionnée dans le dossier du projet.  Il est utile "
"pour localiser rapidement tous les endroits où une vidéo est utilisée sur la "
"frise chronologique."

#: ../../user_interface/menu/clip_menu/clip_in_timeline.rst:28
msgid ""
"Selecting the :menuselection:`Clip In Timeline` menu item brings up a flyout "
"that lists all instances of the selected clip, identified by their track and "
"position on the timeline. Clicking on an entry in the list will reposition "
"the playhead to the beginning of indicated clip."
msgstr ""
"La sélection de l'élément de menu :menuselection:`Vidéo dans la frise "
"chronologique` fait apparaître un menu déroulant listant toutes les "
"instances de la vidéo sélectionnée, identifiées par leur piste et leur "
"position sur la frise chronologique. Un clic sur une entrée de la liste vous "
"permettra de repositionner la tête de lecture au début de la vidéo indiquée."

#: ../../user_interface/menu/clip_menu/clip_in_timeline.rst:30
msgid ""
"In the example, we have clicked on the third video entry which is located on "
"video track 1 at the 00:35;09 mark and the playhead is now located at the "
"start of that clip."
msgstr ""
"Dans l'exemple, un clic est fait sur la troisième entrée vidéo se trouvant "
"sur la piste vidéo 1 à la marque 00:35 ; 09. La tête de lecture est "
"maintenant positionnée au début de cette vidéo."

#: ../../user_interface/menu/clip_menu/clip_in_timeline.rst:32
msgid ""
"This option will be greyed out if the clip is not being used in the timeline."
msgstr ""
"Cette option sera grisée si la vidéo n'est pas utilisée dans la frise "
"chronologique."

#: ../../user_interface/menu/clip_menu/clip_in_timeline.rst:34
msgid ""
"See also :menuselection:`Clip in Project Bin` found by :ref:"
"`right_click_menu` on a clip in the timeline."
msgstr ""
"Veuillez consulter :menuselection:`Vidéo dans le dossier du projet`, "
"accessible par :ref:`menu_clic_droit` sur une vidéo dans la frise "
"chronologique."
