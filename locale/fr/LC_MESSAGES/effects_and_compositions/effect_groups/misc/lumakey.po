# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-06 15:05+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:11
msgid "Lumakey Effect"
msgstr "Effet Lumakey"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:13
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:15
msgid "In version 15.n of Kdenlive this is in the Misc category of effects."
msgstr ""
"Dans la version 15.n de Kdenlive, ceci est présent dans la catégorie "
"« Divers » des effets."

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:17
msgid ""
"The Lumakey effect changes the clip's alpha channel. To see its effect, you "
"need a transition (like the Composite transition that is available in "
"tracks) and another clip beneath."
msgstr ""
"L'effet « Lumakey » modifie le canal alpha de la vidéo. Pour voir son effet, "
"vous avez besoin d'une transition (comme la transition « Composite » "
"disponible dans les pistes) et d'une autre vidéo en dessous."

#~ msgid "Available in the Misc category of effects in version 15.n."
#~ msgstr ""
#~ "Disponible dans la catégorie des effets divers dans la version 15.n."
