msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-04-26 15:21\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_user_interface___menu___project_menu___clean_project.pot\n"
"X-Crowdin-File-ID: 26225\n"

#: ../../user_interface/menu/project_menu/clean_project.rst:15
msgid "Clean Project"
msgstr ""

#: ../../user_interface/menu/project_menu/clean_project.rst:17
msgid "Contents"
msgstr ""

#: ../../user_interface/menu/project_menu/clean_project.rst:19
msgid ""
"Available from the :ref:`project_menu` menu this function removes any clips "
"from the Project Bin that are not currently being used on the timeline. The "
"files remain on the hard drive and are only removed from the Project Bin."
msgstr ""

#: ../../user_interface/menu/project_menu/clean_project.rst:21
msgid "You can undo this action with :kbd:`Ctrl + Z`."
msgstr ""

#: ../../user_interface/menu/project_menu/clean_project.rst:25
msgid ""
"This is different from the :ref:`project_settings` button on the Project "
"Files tab in Project Settings which deletes files not used by the project "
"from the hard drive."
msgstr ""
