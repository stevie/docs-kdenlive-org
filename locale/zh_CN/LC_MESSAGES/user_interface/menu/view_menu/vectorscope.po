msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-01 00:38+0000\n"
"PO-Revision-Date: 2022-04-26 15:21\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_user_interface___menu___view_menu___vectorscope.pot\n"
"X-Crowdin-File-ID: 26061\n"

#: ../../user_interface/menu/view_menu/vectorscope.rst:14
msgid "Vectorscope Window"
msgstr ""

#: ../../user_interface/menu/view_menu/vectorscope.rst:16
msgid "Contents"
msgstr ""

#: ../../user_interface/menu/view_menu/vectorscope.rst:18
msgid ""
"This window allows you to monitor the colour properties of your clip in "
"detail."
msgstr ""

#: ../../user_interface/menu/view_menu/vectorscope.rst:20
msgid ""
"The Vectorscope shows the hue and saturation distribution in a way we can "
"understand without problems. This is useful for quickly recognizing color "
"casts, but also helps judging the color distribution of a clip and matching "
"it to others."
msgstr ""

#: ../../user_interface/menu/view_menu/vectorscope.rst:22
msgid ""
":ref:`See Granjow's blog here <vectorscope_working>` and :ref:`here "
"<vectorscope_i-q_lines>` on the Vectorscope."
msgstr ""

#: ../../user_interface/menu/view_menu/vectorscope.rst:30
msgid ":ref:`scopes_directx`"
msgstr ""
