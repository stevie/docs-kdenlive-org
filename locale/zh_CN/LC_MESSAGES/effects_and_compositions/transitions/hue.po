msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-04-26 15:21\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___transitions___hue.pot\n"
"X-Crowdin-File-ID: 25905\n"

#: ../../effects_and_compositions/transitions/hue.rst:12
msgid "Hue"
msgstr ""

#: ../../effects_and_compositions/transitions/hue.rst:14
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/transitions/hue.rst:16
msgid ""
"This is the `Frei0r hue <https://www.mltframework.org/plugins/"
"TransitionFrei0r-hue/>`_ MLT transition."
msgstr ""

#: ../../effects_and_compositions/transitions/hue.rst:18
msgid ""
"Perform a conversion to hue only of the source input1 using the hue of "
"input2."
msgstr ""
