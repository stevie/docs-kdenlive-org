msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-04-26 15:21\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___analysis_and_data___video_values."
"pot\n"
"X-Crowdin-File-ID: 26109\n"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/video_values.rst:10
msgid "Video Values"
msgstr "Video Values - 视频明度"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/video_values.rst:12
msgid "Contents"
msgstr "目录"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/video_values.rst:14
msgid ""
"This is the `Frei0r pr0be <https://www.mltframework.org/plugins/FilterFrei0r-"
"pr0be/>`_ MLT filter."
msgstr ""
"这是 `Frei0r pr0be <https://www.mltframework.org/plugins/FilterFrei0r-pr0be/"
">`_ MLT 滤镜。"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/video_values.rst:16
msgid "Measures video values."
msgstr "测量视频明度."
