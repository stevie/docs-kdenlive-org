# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 12:51+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:12
msgid "Delay grab effect"
msgstr "Vertraagd grijpeffect"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:14
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:16
msgid "This effect is available from the misc group."
msgstr "Dit effect is beschikbaar uit de groep diversen."

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:18
msgid ""
"This is the `Frei0r delaygrab <https://www.mltframework.org/plugins/"
"FilterFrei0r-delaygrab/>`_ MLT filter by Bill Spinhover, Andreas Scheffler "
"and Jaromil."
msgstr ""
"Dit is het MLT-filter `Frei0r vertraagd grijpen <https://www.mltframework."
"org/plugins/FilterFrei0r-delaygrab/>`_ door Bill Spinhover, Andreas "
"Scheffler en Jaromil."

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:20
msgid "Delayed frame blitting mapped on a time bitmap."
msgstr "Vertraagde framevervorming afgebeeld op een tijdbitmap."

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:22
msgid "https://youtu.be/t6rsEdDiuAQ"
msgstr "https://youtu.be/t6rsEdDiuAQ"

#: ../../effects_and_compositions/effect_groups/misc/delay_grab.rst:24
msgid "https://youtu.be/vh63RxHm8Lg"
msgstr "https://youtu.be/vh63RxHm8Lg"
