# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-27 12:13+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/transitions/addition.rst:11
msgid "addition transition"
msgstr "overgang toevoegen"

#: ../../effects_and_compositions/transitions/addition.rst:13
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/transitions/addition.rst:15
msgid ""
"This is the `Frei0r addition <https://www.mltframework.org/plugins/"
"TransitionFrei0r-addition/>`_ MLT transition."
msgstr ""
"Dit is de MLT-overgang `Frei0r addition <https://www.mltframework.org/"
"plugins/TransitionFrei0r-addition/>`_."

#: ../../effects_and_compositions/transitions/addition.rst:17
msgid "Perform an RGB[A] addition operation of the pixel sources."
msgstr "Voer een RGB[A] bewerking toevoeging uit op de pixelbronnen."
