# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-02-22 16:14+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:12
msgid "Alpha operation transitions"
msgstr "Transities met alfa-bewerking"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:14
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:16
msgid ""
"The following transitions all perform alpha operations between the two video "
"tracks:"
msgstr ""
"De volgende overgangen voeren allemaal alfa-bewerkingen tussen de twee "
"videotracks uit:"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:18
msgid ":ref:`addition_alpha`"
msgstr ":ref:`addition_alpha`"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:19
msgid ":ref:`addition`"
msgstr ":ref:`addition`"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:20
#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:58
msgid ":ref:`alphaatop`"
msgstr ":ref:`alphaatop`"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:21
#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:52
msgid ":ref:`alphain`"
msgstr ":ref:`alphain`"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:22
#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:46
msgid ":ref:`alphaout`"
msgstr ":ref:`alphaout`"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:23
#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:64
msgid ":ref:`alphaover`"
msgstr ":ref:`alphaover`"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:24
#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:40
msgid ":ref:`alphaxor`"
msgstr ":ref:`alphaxor`"

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:26
msgid ""
"These transitions only have an effect if the videos on the tracks have alpha "
"channel information in them."
msgstr ""
"Deze transities hebben alleen een effect als de video's in de tracks alpha-"
"kanaalinformatie in zich hebben."

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:28
msgid ""
"The Alpha Channel information is supplied by one of the :ref:"
"`alpha_manipulation`. This Alpha Channel data describes which regions of the "
"video track is transparent and how transparent it should be. Until you "
"define some alpha channel data using an :ref:`alpha_manipulation` changes in "
"the alpha operation transition settings will have no visible effect."
msgstr ""
"De Alfakanaalinformatie wordt geleverd door een van de :ref:"
"`alpha_manipulation`. Deze alfakanaalgegevens beschrijven welke gebieden van "
"de videotrack transparent zijn en hoe transparent deze zouden moeten zijn. "
"Totdat u enige alfakanaalgegevens definieert met een :ref:"
"`alpha_manipulation` zullen wijzigingen in de instellingen voor "
"alfabewerkingsovergangen geen zichtbaar effect hebben."

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:30
msgid ""
"The alpha operation transitions define how the two different alpha channel "
"information should be combined to produce the final image. These operations "
"are implementing the operations described at Wikipedia page on `Alpha "
"Compositing <https://en.wikipedia.org/wiki/Alpha_compositing>`_."
msgstr ""
"De alfabewerkingsovergangen definiëren hoe de twee verschillende "
"alfakanaalinformatie gecombineerd moeten worden om de uiteindelijke "
"afbeelding te produceren. Deze bewerkingen implementeren de bewerkingen "
"beschreven op de Wikipedia-pagina over `Alfa Compositing <https://en."
"wikipedia.org/wiki/Alpha_compositing>`_."

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:32
msgid ""
"In the examples below the yellow clip has a triangle alpha shape with min=0 "
"and max=618. This translates to 0% opacity outside the triangle and 61.8% "
"opacity inside the triangle. Ie the alpha channel in the yellow track say "
"show all the track underneath outside the triangle and show 38.2% of the "
"underneath track inside the triangle."
msgstr ""
"In de voorbeelden onder de gele clip is een driehoekige alfavorm met min=0 "
"en max=618. Dit vertaalt naar 0% dekking buiten de driehoek en 61,8% dekking "
"in de driehoek. Dus het alfakanaal in de gele track zegt toon alles van de "
"track onder de buitenkant van de driehoek and toon 38,2% onder de binnenkant "
"van de driehoek."

#: ../../effects_and_compositions/transitions/alpha_operation_transitions.rst:34
msgid ""
"The Green clip has a rectangle alpha shape with min=0 and max=1000. This "
"translates to make the clip 100% transparent outside the rectangle and 0% "
"transparent inside the rectangle."
msgstr ""
"De groene clip heeft een rechthoekige alfavorm met min=0 en max=1000. Dit "
"vertaalt naar het maken van de clip 100% transparant buiten de rechthoek en "
"0% transparant binnen de rechthoek."
