# Translation of docs_kdenlive_org_effects_and_compositions___transitions___addition_alpha.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 13:00+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/transitions/addition_alpha.rst:11
msgid "Addition_alpha transition"
msgstr "Transició d'addició alfa"

#: ../../effects_and_compositions/transitions/addition_alpha.rst:13
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/transitions/addition_alpha.rst:15
msgid ""
"This is the `Frei0r addition_alpha <https://www.mltframework.org/plugins/"
"TransitionFrei0r-addition_alpha/>`_ MLT transition."
msgstr ""
"Aquesta és la `transició «addition_alpha» de Frei0r <https://www."
"mltframework.org/plugins/TransitionFrei0r-addition_alpha/>`_ del MLT."

#: ../../effects_and_compositions/transitions/addition_alpha.rst:17
msgid "Perform an RGB[A] addition_alpha operation of the pixel sources."
msgstr "Realitza una operació «RGB[A] addition_alpha» de les fonts de píxels."
