# Translation of docs_kdenlive_org_glossary___useful_information___insert_overwrite_advanced_timeline_editing.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-19 00:37+0000\n"
"PO-Revision-Date: 2022-01-24 15:21+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:13
msgid "Insert and Overwrite: advanced timeline editing"
msgstr "Inserció i sobreescriptura: edició avançada de la línia de temps"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:17
msgid ""
"Kdenlive offers advanced timeline editing functions. In this article we’re "
"looking at the :menuselection:`insert` |timeline-insert| and :menuselection:"
"`overwrite` |timeline-overwrite| advanced editing functions. A later article "
"then will cover the opposite :menuselection:`lift` |timeline-lift| and :"
"menuselection:`extract` |timeline-extract| functions."
msgstr ""
"El Kdenlive ofereix funcions avançades d'edició de la línia de temps. En "
"aquest article veurem les funcions d'edició avançada :menuselection:"
"`inserció` |timeline-insert| i :menuselection:`sobreescriptura` |timeline-"
"overwrite|. Un article posterior cobrirà les funcions :menuselection:"
"`elevació` |timeline-lift| i :menuselection:`extracció` |timeline-extract|."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:19
msgid ""
"When inserting or overwriting some part in the timeline with some part from "
"a clip, we now face two zones, so how does this work out at all? We only "
"want to deal with three points, that is, with one zone and a point (for that "
"reason this is also sometimes termed three point editing). In consequence, "
"there are two different insert/overwrite operations:"
msgstr ""
"En inserir o sobreescriure una part de la línia de temps amb una part d'un "
"clip, ens enfrontem a dues zones, així que com funciona això? Només volem "
"tractar tres punts, és a dir, amb una zona i un punt (per aquesta raó, a "
"vegades també es denomina edició de tres punts). En conseqüència, hi ha dues "
"operacions d'inserció i sobreescriptura diferents:"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:21
msgid ""
"insert/overwrite a clip zone into timeline at some point (cursor/playhead), "
"or"
msgstr ""
"inserció/sobreescriptura d'una zona de clip en la línia de temps en algun "
"punt (cursor/capçal de reproducció), o"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:22
msgid "insert/overwrite a clip starting at some point into a timeline zone."
msgstr ""
"inserció/sobreescriptura d'un clip que comenci en algun punt en una zona de "
"la línia de temps."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:25
msgid "Insert Clip Zone into Timeline at Timeline Cursor"
msgstr ""
"Inserció d'una zona de clip en la línia de temps al cursor de la línia de "
"temps"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:35
msgid ""
"As we’re going to insert a clip zone into the timeline, first make sure that "
"the :menuselection:`switch for using timeline zone is crossed out` |timeline-"
"use-zone-off| (it’s *off*). This is also shown in the screenshot. (You’ll "
"find this switch above the track headers, next to the track size zoom in/out "
"controls.)"
msgstr ""
"Com que inserirem una zona de clip en la línia de temps, primer assegureu-"
"vos que el :menuselection:`commutador per usar la zona de la línia de temps "
"està barrat` |timeline-use-zone-off| (està *apagat*). Això també es mostra a "
"la captura de pantalla. ( Trobareu aquest commutador per sobre de les "
"capçaleres de la pista, al costat dels controls de zoom de la mida de la "
"pista)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:37
msgid ""
"A visual clue (albeit a rather unintrusive one) is that the **timeline zone "
"bar** is now *dimmed*."
msgstr ""
"Una pista visual (encara que força no intrusiva) és que la **barra de zona "
"de la línia de temps** ara està *atenuada*."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:46
msgid ""
"Next, mark the **clip region** of the source clip you want to insert into "
"the timeline. You do this as usual, using either the :kbd:`I` and :kbd:`O` "
"shortcuts, or the set zone in/out buttons of the clip monitor."
msgstr ""
"A continuació, marqueu la **regió del clip** del clip d'origen que voleu "
"inserir a la línia de temps. Ho feu com de costum, utilitzant les dreceres :"
"kbd:`I` i :kbd:`O`, o els botons d'entrada/sortida de zona del monitor de "
"clips."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:55
msgid ""
"Now **place the timeline cursor** to where you want to start with the insert."
msgstr ""
"Ara **situeu el cursor de la línia de temps** a on voleu començar amb la "
"inserció."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:57
msgid ""
"Also make sure to **select the correct track** for insertion – using the :"
"kbd:`cursor up` and :kbd:`cursor down` keys. (Remember that the currently "
"selected track is marked with the semi-transparent selection color, the "
"exact color of which depends on your particular color theme.)"
msgstr ""
"També assegureu-vos de **seleccionar la pista correcta** per a la inserció, "
"utilitzant les tecles :kbd:`fletxa amunt` i :kbd:`fletxa avall`. (Recordeu "
"que la pista actualment seleccionada està marcada amb el color de selecció "
"semitransparent, el color exacte del qual depèn del tema de color en "
"particular)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:66
msgid ""
"Finally press the :kbd:`v` shortcut, or click the :menuselection:`insert "
"clip zone toolbar button` |timeline-insert|, or use :menuselection:`Timeline "
"--> Insertion --> Insert Clip Zone in Timeline`."
msgstr ""
"Finalment, premeu la drecera :kbd:`v`, o feu clic al :menuselection:`botó de "
"la barra d'eines d'inserció de zona del clip` |timeline-insert|, o "
"utilitzeu :menuselection:`Línia de temps --> Inserció --> Insereix una zona "
"del clip a la línia de temps`."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:72
msgid ""
"Insertion starts from the timeline cursor, and not from the timeline zone "
"start (because we chose to ignore it in our very first step)."
msgstr ""
"La inserció comença des del cursor de la línia de temps, i no des de l'inici "
"de la zona de la línia de temps (perquè hem triat ignorar-la en el primer "
"pas)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:73
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:124
msgid "Locked tracks are unaffected, such as the topmost track in our example."
msgstr ""
"Les pistes bloquejades no es veuen afectades, com la pista de més amunt del "
"nostre exemple."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:74
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:125
msgid ""
"Unlocked tracks get affected in that whatever is at the insertion point and "
"later in the timeline gets shifted away to make room for the insertion."
msgstr ""
"Les pistes desbloquejades es veuen afectades en el punt d'inserció i més "
"endavant en la línia de temps es mouen enllà per fer lloc a la inserció."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:77
msgid "Insert Clip (from In Point) into Timeline Zone"
msgstr ""
"Insereix un clip (des del punt d'entrada) a la zona de la línia de temps"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:86
msgid ""
"This time, we’re going to insert some part of a clip to *exactly fit* into "
"the timeline zone. So we now need to switch on using the :menuselection:"
"`timeline zone` |timeline-use-zone-on|. This is also shown in the "
"screenshot. (You’ll find this switch above the track headers, next to the "
"track size zoom in/out controls.)"
msgstr ""
"Aquesta vegada, inserirem una part d'un clip per *ajustar-lo exactament* a "
"la zona de la línia de temps. Així que ara hem d'activar l'ús de la :"
"menuselection:`zona de la línia de temps` |timeline-use-zone-on|. Això també "
"es mostra a la captura de pantalla. ( Trobareu aquest commutador per sobre "
"de les capçaleres de la pista, al costat dels controls de zoom de la mida de "
"la pista)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:88
msgid ""
"A visual clue (albeit a rather unintrusive one) is that the **timeline zone "
"bar** is now *bright*."
msgstr ""
"Una pista visual (encara que força no intrusiva) és que la **barra de zona "
"de la línia de temps** ara és *brillant*."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:97
msgid ""
"This time, we only need to **set the in point** for our source clip. The out "
"point doesn’t matter, as it will be later determined automatically by the "
"length of the timeline zone."
msgstr ""
"Aquesta vegada, només hem de **establir el punt d'entrada** per al nostre "
"clip d'origen. El punt de sortida no importa, ja que més endavant es "
"determinarà automàticament per la longitud de la zona de la línia de temps."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:106
msgid ""
"Now, place **mark the timeline zone** into which you want to insert a part "
"of your source clip. Notice that the timeline cursor position now doesn’t "
"matter."
msgstr ""
"Ara, situeu la **marca de la zona de la línia de temps** en la qual voleu "
"inserir una part del vostre clip d'origen. Tingueu en compte que ara la "
"posició del cursor de la línia de temps no importa."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:108
msgid ""
"Make sure to **select the correct track** for insertion – using the :kbd:"
"`cursor up` and :kbd:`cursor down` keys."
msgstr ""
"Assegureu-vos de **seleccionar la pista correcta** per a la inserció, "
"utilitzant les tecles :kbd:`fletxa amunt` i :kbd:`fletxa avall`."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:117
msgid ""
"Finally press the :kbd:`V` shortcut, or click the :menuselection:`insert "
"clip zone toolbar button` |timeline-insert|, or use :menuselection:`Timeline "
"--> Insertion --> Insert Clip Zone in Timeline`."
msgstr ""
"Finalment, premeu la drecera :kbd:`V`, o feu clic al :menuselection:`botó de "
"la barra d'eines d'inserció de zona del clip` |timeline-insert|, o "
"utilitzeu :menuselection:`Línia de temps --> Inserció --> Insereix una zona "
"del clip a la línia de temps`."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:123
msgid ""
"Insertion starts from the beginning of the timeline zone, and not from the "
"timeline cursor position (because we chose to enable the timeline zone in "
"our very first step)."
msgstr ""
"La inserció comença des del principi de la zona de la línia de temps, i no "
"des de la posició del cursor de la línia de temps (perquè hem triat activar "
"la zona de la línia de temps en el primer pas)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:129
msgid "Overwrite Timeline with Clip Zone"
msgstr "Sobreescriu la línia de temps amb una zona de clip"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:131
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:138
msgid ":menuselection:`overwrite` |timeline-overwrite|"
msgstr ":menuselection:`sobreescriu` |timeline-overwrite|"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:133
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:140
msgid "(will be documented later)"
msgstr "(es documentarà més endavant)"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:136
msgid "Overwrite Timeline Zone with Clip"
msgstr "Sobreescriu la zona de la línia de temps amb un clip"
