# Translation of docs_kdenlive_org_user_interface___menu___view_menu___clip_monitor.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-19 10:07+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/view_menu/clip_monitor.rst:14
msgid "Clip Monitor"
msgstr "Monitor de clips"

#: ../../user_interface/menu/view_menu/clip_monitor.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/view_menu/clip_monitor.rst:18
msgid "Toggles the display of the :ref:`monitors`."
msgstr "Mostra/oculta la visualització dels :ref:`monitors`."
