# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-30 00:37+0000\n"
"PO-Revision-Date: 2022-01-30 21:46+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../user_interface/menu/view_menu/screen_grab.rst:11
msgid "Screen Grab"
msgstr "Skärmlagring"

#: ../../user_interface/menu/view_menu/screen_grab.rst:16
msgid "Contents"
msgstr "Innehåll"

#: ../../user_interface/menu/view_menu/screen_grab.rst:24
msgid "Start recording: click the “record” button."
msgstr "Starta inspelning: klicka på knappen \"spela in\"."

#: ../../user_interface/menu/view_menu/screen_grab.rst:27
msgid "Stop record: click the \"record\" button again."
msgstr "Stoppa inspelning: klicka på knappen \"spela in\" igen."

#: ../../user_interface/menu/view_menu/screen_grab.rst:30
msgid "For more information see :ref:`screen_grab_capturing`"
msgstr "För mer information, se :ref:`screen_grab_capturing`"

#: ../../user_interface/menu/view_menu/screen_grab.rst:33
msgid ""
"Clicking on the configure button brings you to the :ref:`configure_kdenlive` "
"window."
msgstr ""
"Att klicka på inställningsknappen går till fönstret  :ref:"
"`configure_kdenlive`."
