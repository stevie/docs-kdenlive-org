# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 17:21+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/effect_groups/misc/edge_glow.rst:15
msgid "Edge Glow"
msgstr "Kantglöd"

#: ../../effects_and_compositions/effect_groups/misc/edge_glow.rst:17
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/effect_groups/misc/edge_glow.rst:19
msgid ""
"This is the `Frie0r edgeglow <https://www.mltframework.org/plugins/"
"FilterFrei0r-edgeglow/>`_ MLT filter."
msgstr ""
"Det här är MLT-filtret `Frie0r kantglöd <https://www.mltframework.org/"
"plugins/FilterFrei0r-edgeglow/>`_."

#: ../../effects_and_compositions/effect_groups/misc/edge_glow.rst:21
msgid "In version 15.n of Kdenlive this is in the Blur and Hide section."
msgstr "I Kdenlive version 15 finns det i sektionen Suddighet och Dölj."

#: ../../effects_and_compositions/effect_groups/misc/edge_glow.rst:23
msgid "https://youtu.be/d0MvA_7VuJk"
msgstr "https://youtu.be/d0MvA_7VuJk"

#: ../../effects_and_compositions/effect_groups/misc/edge_glow.rst:25
msgid "https://youtu.be/Cl0Z8FXULbQ"
msgstr "https://youtu.be/Cl0Z8FXULbQ"
